package de.senatov.spring.kafka.pandcdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PAndCDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PAndCDemoApplication.class, args);
	}

}
