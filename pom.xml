<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.4.2</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>
	<groupId>de.senatov.spring.kafka</groupId>
	<artifactId>p-and-c-demo</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>p-and-c-demo</name>
	<description>Kafka producer and consumer project for Spring Boot</description>

	<properties>
		<java.version>11</java.version>
		<build-timestamp>${maven.build.timestamp}</build-timestamp>
		<downloadJavadocs>true</downloadJavadocs>
		<downloadSources>true</downloadSources>
		<failOnMissingWebXml>false</failOnMissingWebXml>
		<maven.build.timestamp.format>yyyy-MM-dd HH:mm</maven.build.timestamp.format>
		<maven.compiler.source>11</maven.compiler.source>
		<maven.compiler.target>11</maven.compiler.target>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<log4j.ver>2.11.2</log4j.ver>
	</properties>

	<developers>
		<developer>
			<id>senatov</id>
			<name>Iakov Senatov</name>
			<email>javaentwickler@gmail.com</email>
			<organization>freelance</organization>
			<roles>
				<role>developer</role>
			</roles>
		</developer>
	</developers>

	<repositories>
		<repository>
			<id>JBoss-Repo</id>
			<url>https://repository.jboss.org/nexus/content/repositories/releases/</url>
		</repository>
		<repository>
			<id>jitpack</id>
			<url>https://jitpack.io</url>
		</repository>
		<repository>
			<id>jvnet-nexus-snapshots</id>
			<name>jvnet-nexus-snapshots</name>
			<url>https://maven.java.net/content/repositories/snapshots/</url>
		</repository>
		<repository>
			<id>prime-repo</id>
			<name>PrimeFaces Maven Repository</name>
			<url>http://repository.primefaces.org</url>
		</repository>
		<repository>
			<id>repo1.maven.org</id>
			<url>https://repo1.maven.org/maven2/</url>
		</repository>
		<repository>
			<id>sonatype-snapshots</id>
			<name>Sonatype Snapshots</name>
			<url>https://oss.sonatype.org/content/repositories/snapshots/</url>
		</repository>
		<repository>
			<id>spring-milestones</id>
			<name>Spring Milestones</name>
			<url>https://repo.spring.io/milestone</url>
		</repository>
		<repository>
			<id>spring-snapshots</id>
			<name>Spring Snapshots</name>
			<url>https://repo.spring.io/snapshot</url>
		</repository>
	</repositories>

	<pluginRepositories>
		<pluginRepository>
			<id>sonatype-snapshots</id>
			<name>Sonatype Snapshots</name>
			<url>https://oss.sonatype.org/content/repositories/snapshots/</url>
		</pluginRepository>
		<pluginRepository>
			<id>spring.io.plugins-snapshot</id>
			<name>spring.io.plugins-snapshot</name>
			<url>https://repo.spring.io/plugins-snapshot</url>
		</pluginRepository>
	</pluginRepositories>

	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<dependency>
			<groupId>org.apache.kafka</groupId>
			<artifactId>kafka-streams</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.kafka</groupId>
			<artifactId>spring-kafka</artifactId>
		</dependency>

		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<optional>true</optional>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework.kafka</groupId>
			<artifactId>spring-kafka-test</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework.restdocs</groupId>
			<artifactId>spring-restdocs-mockmvc</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-jdk14</artifactId>
			<version>1.7.28</version>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<artifactId>maven-war-plugin</artifactId>
				<version>3.3.0</version>
				<configuration>
					<archive>
						<manifest>
							<addClasspath>true</addClasspath>
							<addDefaultImplementationEntries>true</addDefaultImplementationEntries>
							<classpathPrefix>lib/</classpathPrefix>
						</manifest>
						<manifestEntries>
							<Build-Jdk>${java.version} (${java.vendor} ${java.vm.version}</Build-Jdk>
							<Build-OS>${os.name} ${os.arch} ${os.version} ${oauth2-oidc-sdk.version}</Build-OS>
							<Build-Revision>${git.commit.id}</Build-Revision>
							<Build-Timestamp>${maven.build.timestamp}</Build-Timestamp>
							<Built-At>${maven.build.timestamp}</Built-At>
							<Implementation-Build>${project.build}</Implementation-Build>
							<mode>development</mode>
							<url>${project.url}</url>
							<version>${project.version}</version>
						</manifestEntries>
					</archive>
					<failOnMissingWebXml>false</failOnMissingWebXml>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.asciidoctor</groupId>
				<artifactId>asciidoctor-maven-plugin</artifactId>
				<version>2.0.0-RC.1</version>
				<executions>
					<execution>
						<id>generate-docs</id>
						<phase>prepare-package</phase>
						<goals>
							<goal>process-asciidoc</goal>
						</goals>
						<configuration>
							<backend>html</backend>
							<doctype>book</doctype>
						</configuration>
					</execution>
				</executions>
				<dependencies>
					<dependency>
						<groupId>org.springframework.restdocs</groupId>
						<artifactId>spring-restdocs-asciidoctor</artifactId>
						<version>${spring-restdocs.version}</version>
					</dependency>
				</dependencies>
			</plugin>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<configuration>
					<excludes>
						<exclude>
							<groupId>org.projectlombok</groupId>
							<artifactId>lombok</artifactId>
						</exclude>
					</excludes>
				</configuration>
			</plugin>
		</plugins>
	</build>

</project>
